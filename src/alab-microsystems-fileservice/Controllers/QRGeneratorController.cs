﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace alab_microsystems_fileservice.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    public class QRGeneratorController : BaseController
    {
        readonly IUploadService FileServiceProvider;


        public QRGeneratorController(IUploadService fileServiceProvider)
        {
            FileServiceProvider = fileServiceProvider;
        }



        [HttpPost("QR")]
        public async Task<IActionResult> GetQRCode([FromQuery] QROptions options, [FromBody]object data)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(data.ToString(),
            QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);

            var result = await FileServiceProvider.Upload(options.AppCode, "QR", options.Id, qrCodeImage);
            return Ok(result);
        }

        private static byte[] BitmapToBytes(Bitmap img)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }


    }
}
