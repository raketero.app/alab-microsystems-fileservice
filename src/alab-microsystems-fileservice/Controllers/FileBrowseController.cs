﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace alab_microsystems_fileservice.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    public class FileBrowseController : BaseController
    {

        readonly IFileBrowseService FileServiceProvider;

        //test
        public FileBrowseController(IFileBrowseService fileServiceProvider)
        {
            FileServiceProvider = fileServiceProvider;
        }


        /// <summary>
        /// View Directory Files
        /// </summary>
        /// <param name="appcode"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("browse/{appcode}/{id}")]
        public async Task<IActionResult> UploadAvatar(string appcode, string id)
        {
            return Ok(FileServiceProvider.GetFiles(appcode, id));

        }
    }
}
