﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alab_microsystems_fileservice
{
    public class UploadResultBatch
    {
        public int Uploaded { get; }
        public int Failed { get; }
        public DateTime TimeStamp { get; }

        public List<FileSet> Files { get; set; }

        public UploadResultBatch(int uploaded, int failed, DateTime timeStamp, List<FileSet> files)
        {
            Uploaded = uploaded;
            Failed = failed;
            TimeStamp = timeStamp;
            Files = files;
        }
    }
}
