﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alab_microsystems_fileservice
{
    public class UploadResult
    {
        public int Uploaded { get; }
        public int Failed { get; }
        public DateTime TimeStamp { get; }
        public FileSet File { get; set; }

        public UploadResult(int uploaded, int failed, DateTime timeStamp, FileSet file)
        {
            Uploaded = uploaded;
            Failed = failed;
            TimeStamp = timeStamp;
            File = file;
        }
    }
}
