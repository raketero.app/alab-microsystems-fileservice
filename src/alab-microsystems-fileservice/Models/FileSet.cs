﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alab_microsystems_fileservice
{
    public class FileSet
    {
        public string AppCode { get; set; }
        public string Group { get; set; }
        public string Id { get; set; }
        public string FileType { get; set; }
        public string Path { get; set; }
    }
}
