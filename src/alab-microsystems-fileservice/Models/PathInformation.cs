﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alab_microsystems_fileservice
{
    public class PathInformation
    {
        public string Group { get; }
        public string Path { get; }

        public PathInformation(string path, string group)
        {
            Group = group;
            Path = path;
        }

    }
}
