﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace alab_microsystems_fileservice
{
    public interface IFileBrowseService
    {
        public List<PathInformation> GetFiles(string appcode, string id);
    }

    public class FileBrowseService : IFileBrowseService
    {
        private string _basePath;
        private string _baseUrlPath;


        public List<PathInformation> GetFiles(string appcode, string id)
        {
            var tempList = new List<PathInformation>();

            var options = new EnumerationOptions();

            options.RecurseSubdirectories = true;

            var allfiles = Directory.GetFiles(_basePath, "*.*", options);


            allfiles.ToList().ForEach(x =>
            {

                var needed_path = x.Remove(0, _basePath.Length);


                var subs = needed_path.Split("\\");

                var url = _baseUrlPath;

                foreach(var p in subs)
                {
                    url += $"/{p}";
                }

                var app = subs[0];
                var group = subs[1];
                var user_id = subs[2];
                var details = new PathInformation(url,group);

                if(app == appcode && user_id == id)
                {
                    tempList.Add(details);
                }


            });

            return tempList;
        }

        public FileBrowseService UsingRootPath(string path, string baseUrlPath)
        {
            _basePath = path;
            _baseUrlPath = baseUrlPath;
            return this;
        }

    }
}
