﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace alab_microsystems_fileservice
{
    public interface IUploadService
    {
        Task<UploadResult> UploadAvatar(string code, string id, IFormFile files);
        Task<UploadResult> Upload(string code, string groupId, string id, IFormFile file);
        Task<UploadResult> Upload(string code, string groupId, string id, Bitmap file);
        Task<UploadResultBatch> UploadBatch(string code, string groupId, string id, List<IFormFile> files);
    }

    public class UploadService : IUploadService
    {
        private string _basePath;
        private string _baseUrlPath;


        public async Task<UploadResult> Upload(string code, string groupId, string id, IFormFile file)
        {
            int success = 0;
            int failed = 0;
            var fileHash = Guid.NewGuid();
            var file_extension = string.Empty;
            try
            {
                var path_to_save = Path.Combine(_basePath, code, groupId, id);

                if (!Directory.Exists(path_to_save))
                {
                    Directory.CreateDirectory(path_to_save);
                }

                file_extension = Path.GetExtension(file.FileName);

                var filePath = Path.Combine(path_to_save, $"{fileHash}{file_extension}");


                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                    success++;
                }
            }
            catch
            {
                failed++;
            }

            return await Task.FromResult(new UploadResult(success, failed, DateTime.Now, new FileSet
            {
                AppCode = code,
                Group = groupId,
                Id = id,
                FileType = file_extension,
                Path = $"{_baseUrlPath}/{code}/{groupId}/{id}/{fileHash}{file_extension}"
            }));
        }


        public async Task<UploadResult> Upload(string code, string groupId, string id, Bitmap file)
        {
            int success = 0;
            int failed = 0;
            var fileHash =  id;
            var file_extension = ".png";

            try
            {
                var path_to_save = Path.Combine(_basePath, code, groupId, id);

                if (!Directory.Exists(path_to_save))
                {
                    Directory.CreateDirectory(path_to_save);
                }

                var filePath = Path.Combine(path_to_save, $"{fileHash}.png");

                file.Save(filePath);
            }
            catch
            {
                failed++;

            }



            return await Task.FromResult(new UploadResult(success, failed, DateTime.Now, new FileSet
            {
                AppCode = code,
                Group = groupId,
                Id = id,
                FileType = file_extension,
                Path = $"{_baseUrlPath}/{code}/{groupId}/{id}/{fileHash}{file_extension}"
            }));
        }





        public async Task<UploadResultBatch> UploadBatch(string code, string groupId, string id, List<IFormFile> files)
        {
            int success = 0;
            int failed = 0;
            var files_set = new List<FileSet>();
            var fileHash = Guid.NewGuid();

            var path_to_save = Path.Combine(_basePath, code, groupId, id);

            if (!Directory.Exists(path_to_save))
            {
                Directory.CreateDirectory(path_to_save);
            }

            foreach(var file in files)
            {
                try
                {
                    var file_extension = Path.GetExtension(file.FileName);
                    var filePath = Path.Combine(path_to_save, $"{fileHash}{file_extension}");

                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                        success++;

                        files_set.Add(new FileSet 
                        {
                            AppCode = code,
                            Group = groupId,
                            Id = id,
                            FileType = file_extension,
                            Path = $"{_baseUrlPath}/{code}/{groupId}/{id}/{fileHash}{file_extension}"
                        });
                    }
                }
                catch
                {
                    failed++;
                }
            

            }
            return await Task.FromResult(new UploadResultBatch(success, failed, DateTime.Now, files_set));
        }

 





        public async Task<UploadResult> UploadAvatar(string code, string id, IFormFile file)
        {

            var file_extension = Path.GetExtension(file.FileName);

            List<string> AllowedExtensions = new List<string>(new string[] { ".png", ".jpg", ".jpeg" });

            if (!AllowedExtensions.Contains(file_extension))
            {
                throw new Exception("Invalid image format");
            }

            int success = 0;
            int failed = 0;

            var path_to_save = Path.Combine(_basePath, code, "profile-picture", id);

            if (!Directory.Exists(path_to_save))
            {
                Directory.CreateDirectory(path_to_save);
            }
            
            var filePath = Path.Combine(path_to_save, $"default.png");
            using (var fileStream = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                try
                {

                    await file.CopyToAsync(fileStream);
                    success++;
                }
                catch
                {
                    failed++;
                }
            }
            return await Task.FromResult(new UploadResult(success, failed, DateTime.Now, new FileSet 
            {
                AppCode = code,
                Group = "profile-picture",
                Id = id,
                FileType = ".png",
                Path = $"{_baseUrlPath}/{code}/profile-picture/{id}/default.png"
            }));
        }



        public UploadService UsingRootPath(string path, string baseUrlPath)
        {
            _basePath = path;
            _baseUrlPath = baseUrlPath;
            return this;
        }

      
    }
}
