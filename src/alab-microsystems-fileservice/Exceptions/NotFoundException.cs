﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alab_microsystems_fileservice
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string key)
           : base($"'{ExceptionBuilder.Split(key)}' not found.")
        {

        }
    }
}
