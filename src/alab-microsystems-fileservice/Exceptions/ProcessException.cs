﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alab_microsystems_fileservice
{
    public class ProcessException : Exception
    {
        public object Request { get; }
        public ProcessException(object request, Exception innerException)
            : base("Exception", innerException)
        {
            Request = request;
        }
    }
}
