﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alab_microsystems_fileservice
{
    public class LogicException : Exception
    {
        public int Code { get; }
        public LogicException(int code, string message)
            : base(message)
        {
            Code = code;
        }
    }
}
