﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alab_microsystems_fileservice
{
    public class InvalidArgumentException : ArgumentException
    {
        public IDictionary<string, string[]> Errors { get; private set; }

        public InvalidArgumentException(string propertyName, string error)
            : base($"Invalid argument values.")
        {
            Errors = new Dictionary<string, string[]>
            {
                { propertyName, new string[] { error } }
            };
        }

        public InvalidArgumentException(IDictionary<string, string[]> errors)
            : base($"Invalid argument values.")
        {
            Errors = errors;
        }

        public InvalidArgumentException(Type type, string propertyName, string error)
            : base($"Invalid {type.Name} argument values.")
        {
            Errors = new Dictionary<string, string[]>
            {
                { propertyName, new string[] { error } }
            };
        }

        public InvalidArgumentException(Type type, IDictionary<string, string[]> errors)
            : base($"Invalid {type.Name} argument values.")
        {
            Errors = errors;
        }
    }
}
