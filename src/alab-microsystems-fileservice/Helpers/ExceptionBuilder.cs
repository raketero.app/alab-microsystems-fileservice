﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alab_microsystems_fileservice
{
    public static class ExceptionBuilder
    {
        public static string Split(string source)
        {
            var result = System.Text.RegularExpressions.Regex.Split(source, @"(?<!^)(?=[A-Z])");

            return string.Join(" ", result);
        }

        //public static InvalidArgumentException InvalidEnumValue(string propertyName, Enum value)
        //{
        //    return new InvalidArgumentException(propertyName, $"'{value}' is an invalid '{Split(propertyName)}' value.");
        //}

        public static InvalidArgumentException NotFoundException(string propertyName)
        {
            return new InvalidArgumentException(propertyName, $"'{Split(propertyName)}' does not exist.");
        }

        //public static LogicException InvalidChangeStatusLogic(Enum from, Enum to)
        //{
        //    return new LogicException($"Changing status from '{from}' to '{to}' is not allowed.");
        //}
    }
}
